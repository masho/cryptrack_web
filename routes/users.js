var router = require('express').Router();
var passport = require('passport');

//A middleware to check if User is logged in
const authCheck = (req, res, next) => {
  if (!req.user) {
    res.redirect('/auth/login');
  } else {
    next();
  }
};

router.get('/', authCheck, (req, res) => {
  res.render('profile', {user: req.user});
});

module.exports = router;
