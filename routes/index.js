var express = require('express');
var coinMarket = require('coinmarketcap-api');
var crypto = require('cryptocurrencies');
var bodyParser = require('body-parser');
var moongoose = require('mongoose');
var passport = require('passport');
var User = require('../models/User');
var router = express.Router();

const client = new coinMarket();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

//A middleware to check if User is logged in
const authCheck = (req, res, next) => {
  if (!req.user) {
    res.redirect('/auth/login');
  } else {
    next();
  }
};

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log(req.user);
  res.render('index', { title: 'Cryptrack', user: req.user});
});

router.get('/add', authCheck, (req, res) => {
    res.render('add', {title: "Add Coin", user: req.user});
});

router.post('/add', (req, res)=>{
  if(req.xhr || req.accepts('json, html') ==='json'){
      let ticker = req.body.code;
      let amount = req.body.amount;
      let allSymbols = crypto.symbols();
      if (allSymbols.includes(ticker.toUpperCase())) {
        client.getTicker({currency: ticker})
        .then(result=>{
            //console.log('Result: ', result);
            User.findOne({loginID: req.user.loginID}).then((record) => {
              record.coins.push({
                name: result.data.name,
                ticker: result.data.symbol,
                currency: 'USD',
                amount: amount
              });
              record.save().then(() => {
                console.log("New coin added Succesfully");
                let priceText = `The price of ${ticker.toUpperCase()} is ${result.data.quotes.USD.price}!`;
                res.send({error: null, price: priceText});
              });
            });
        })
        .catch(console.error)
      } else {
        res.send({message: 'Error'});
      }
  }else{
      res.send({message: 'Ajax data not received'});
  }
});

module.exports = router;
