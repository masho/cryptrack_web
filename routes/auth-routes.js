const router = require('express').Router();
const passport = require('passport');

router.get('/login', (req, res) => {
    res.render('login', {user: req.user, title: 'Login'});
});

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
});

//Facebook Auth Section
router.get('/facebook', passport.authenticate('facebook'));

router.get('/facebook/redirect', passport.authenticate('facebook'), (req, res) => {
    res.redirect('/user');
});

//Twitter Auth Section
router.get('/twitter', passport.authenticate('twitter'));

router.get('/twitter/redirect', passport.authenticate('twitter'), (req, res) => {
    res.redirect('/user');
});

//Google Auth Section
router.get('/google', passport.authenticate('google', {
    scope: ['profile']
}));

router.get('/google/redirect', passport.authenticate('google'), (req, res) => {
    res.redirect('/user');
});

module.exports = router;