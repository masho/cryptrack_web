const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const coinSchema = new Schema({
    name: String,
    ticker: String,
    amount: Number,
    currency: String
});

const userSchema = new Schema({
    username: String,
    loginType: String,
    loginID: String,
    imageUrl: String,
    coins: [coinSchema]
});

const User = mongoose.model('user', userSchema);
module.exports = User;