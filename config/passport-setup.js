const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20');
const FacbookStrategy = require('passport-facebook').Strategy;
const TwitterStrategy = require('passport-twitter').Strategy;
const keys = require('../config/keys');
const User = require('../models/User');

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findById(id, (err, result) => {
        done(null, result);
    });
});

//Facebook Social Login Engine
passport.use(
    new FacbookStrategy({
        clientID: keys.facebook.appID,
        clientSecret: keys.facebook.secret,
        callbackURL: 'https://cryptrack1.herokuapp.com/auth/facebook/redirect'
        //callbackURL: 'http://localhost:3000/auth/facebook/redirect'
    }, (accessToken, refreshToken, profile, done) => {
        /**
         * When you get the profile from FB,
         * check to se if anyone in the DB has the same ID
         * as the one gotten from FB. If there is, Log the person
         * in else Register the person and log the person in.
         */
        User.findOne({loginID: profile.id}).then((currentUser) => {
            if (currentUser) {
                console.log('User Exists');
                done(null, currentUser);
            } else {
                new User({
                    username: profile.displayName,
                    loginType: 'facebook',
                    loginID: profile.id,
                    imageUrl: ''
                }).save().then((newUser) => {
                    console.log('New User saved');
                    done(null, newUser);
                }).catch((err) => {
                    console.log(err);
                });
            }
        });
    })
);

//Google Social Login Engine
passport.use(
    new GoogleStrategy({
        callbackURL: '/auth/google/redirect',
        clientID: keys.google.clientID,
        clientSecret: keys.google.clientSecret
    }, (accessToken, refreshToken, profile, done) => {
        User.findOne({loginID: profile.id}).then((currentUser) => {
            if (currentUser) {
                console.log('User Exits!');
                done(null, currentUser);
            } else {
                new User({
                    username: profile.displayName,
                    loginType: 'google',
                    loginID: profile.id,
                    imageUrl: profile._json.image.url
                }).save().then((newUser) => {
                    console.log('New User Created');
                    done(null, newUser);
                }).catch((err) => {
                    console.log(err);
                });
            }
        })
    })
);

//Twitter Social Login Strategy
passport.use(
    new TwitterStrategy({
        consumerKey: keys.twitter.consumerKey,
        consumerSecret: keys.twitter.consumerSecret,
        callbackURL: 'https://cryptrack1.herokuapp.com/auth/twitter/redirect'
        //callbackURL: 'http://localhost:3000/auth/twitter/redirect'
    }, (accessToken, refreshToken, profile, done) => {
        User.findOne({loginID: profile.id}, (err, currentUser) => {
            if (currentUser) {
                console.log('User Exists!');
                done(null, currentUser);
            } else {
                new User({
                    username: profile._json.name,
                    loginType: 'twitter',
                    loginID: profile.id,
                    imageUrl: profile._json.profile_image_url
                }).save().then((newUser) => {
                    console.log('New User Created');
                    done(null, newUser);
                }).catch((err) => {
                    console.log(err);
                });
            }
        })
    })
);