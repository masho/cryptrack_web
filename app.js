var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var passport = require('passport');
var cookieSession = require('cookie-session');
var keys = require('./config/keys');
var passportSetup = require('./config/passport-setup');
var graphqlHTTP = require('express-graphql');
var cors = require('cors');

var schema = require('./schema/schema');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var authRouter = require("./routes/auth-routes");

var app = express();

app.use(cors());
mongoose.Promise = global.Promise;
app.use(express.static(path.join(__dirname, 'public')));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(cookieSession({
  maxAge: 24*60*60*1000,
  keys: [keys.session.cookieKey]
}));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Initialize passport
app.use(passport.initialize());
app.use(passport.session());

//Routes
app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/auth', authRouter);
//GraphQL section
app.use('/graphql', graphqlHTTP({
  schema,
  graphiql: true
}));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

mongoose.connect(keys.mongodb.remoteDB);
mongoose.connection.once('open', () => {
  console.log('Connected to DB');
});

module.exports = app;
