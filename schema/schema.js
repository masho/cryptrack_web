const request = require('request');
const graphql = require('graphql');
const User = require('../models/User');

const { 
    GraphQLObjectType,
    GraphQLInt,
    GraphQLSchema,
    GraphQLString,
    GraphQLList,
    GraphQLNonNull,
    GraphQLFloat,
    GraphQLID
 } = graphql;

 //Schema Type for User
 const UserType = new GraphQLObjectType({
     name: 'User',
     fields: () => ({
         id: { type: GraphQLID },
         username: { type: GraphQLString },
         loginType: { type: GraphQLString },
         loginID: { type: GraphQLString },
         imageUrl: { type: GraphQLString },
         coins: { type: GraphQLList(CoinType) }
     })
 });

 const TickerType = new GraphQLObjectType({
     name: 'Ticker',
     fields: () => ({
         price: { type: GraphQLInt }
     })
})

 //Schema Type for Coins
 const CoinType = new GraphQLObjectType({
    name: 'Coin',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        ticker: { type: GraphQLString },
        amount: { type: GraphQLInt },
        currency : { type: GraphQLString }
    })
 });

 //Query section
 const RootQuery = new GraphQLObjectType({
     name: 'RootQueryType',
     fields: {
         user: {
             type: UserType,
             args: { id: { type: GraphQLID }},
             resolve(parent, args){
                 return User.findById(args.id);
             }
         },
         users: {
             type: new GraphQLList(UserType),
             resolve(parent, args){
                 return User.find({});
             }
         },
         ticker: {
             type: TickerType,
             args: { name: { type: GraphQLString }},
             resolve(parent, args){
                let url = "https://min-api.cryptocompare.com/data/price?fsym="+ args.name +"&tsyms=USD";
                request(url, (err, resp, body) => {
                    if (err) {
                        return "Error with the ticker";
                    } else {
                        let res = JSON.parse(body);
                        console.log(res.USD);
                        return res;
                    }
                });
             }
         }
     }
 });

 module.exports = new GraphQLSchema({
     query: RootQuery
 });